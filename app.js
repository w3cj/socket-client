var socket = io.connect('http://localhost:3000');
const messages = document.querySelector('#messages');
const input = document.querySelector('#message');

socket.on('refresh', (data) => {
  messages.innerHTML = messages.innerHTML + `<li>${data.message}</li>`;
});

document.querySelector('button').addEventListener('click', () => {
  socket.emit('hello', { message: input.value  });
});
